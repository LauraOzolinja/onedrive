//
//  ViewController.swift
//  OneDrive
//
//  Created by Mark on 01/03/2018.
//  Copyright © 2018 Kaspars_Martins. All rights reserved.
//

import UIKit
import WebKit
class ViewController: UIViewController , WKUIDelegate, WKNavigationDelegate,UITableViewDelegate, UITableViewDataSource {
  
    

   
    @IBOutlet weak var ftable: UITableView!
    @IBOutlet weak var datview: WKWebView!

    var fileNames = [] as Array
    var fileLinks = [] as Array
   
    
var code = ""
  
    
    override func viewDidLoad() {
            super.viewDidLoad()
         
        let site = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=fb36af5b-cb0b-4c4c-87f6-0645de19f299&scope=files.read&response_type=code&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient"
        let url = URL(string:site.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        //code M80cf15f9-475f-ef2f-fa2e-65787e99dbfd
        
        let open = URLRequest(url: url!)
        datview.navigationDelegate = self as WKNavigationDelegate;
        datview.load(open)
        ftable.delegate = self as! UITableViewDelegate
        ftable.dataSource = self as! UITableViewDataSource
        
    }
    
    func numberOfSectionsInTableView(filetable: UITableView) -> Int {
        // 1
        return 1
    }
    
    func tableView(_ filetable: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 2
        return fileNames.count
    }
    
    func tableView(_ filetable: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        var fileURL = fileLinks[indexPath.row] as! String
        print(fileNames[indexPath.row])
    
        
        let filePath = Bundle.main.path(forResource: fileURL, ofType: "jpg")
        
        let folderPath = Bundle.main.path(forAuxiliaryExecutable: "folder")
        
        let fileUrl = NSURL(fileURLWithPath: filePath!)
        let baseUrl = NSURL(fileURLWithPath: folderPath!, isDirectory: true)
        
        datview.loadFileURL(fileUrl as URL, allowingReadAccessTo: baseUrl as URL)
       
        
    }
    
    
    
    func tableView(_ filetable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
        
        cell.textLabel!.text = fileNames[indexPath.row] as! String
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
  
    }
    
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void)
    {
        
        let string = navigationAction.request.url?.absoluteString
        var string_arr = string?.components(separatedBy: "=")
        
        if ( string_arr![0].range(of:"code") != nil ) {
            code = string_arr![1]
            var access_token = ""
            
            let url = URL(string: "https://login.microsoftonline.com/common/oauth2/v2.0/token")!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "client_id=fb36af5b-cb0b-4c4c-87f6-0645de19f299&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient&code=\(code)&grant_type=authorization_code"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                
                let responseString = String(data: data, encoding: String.Encoding.utf8)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                    access_token = json["access_token"] as! String
                    var get_json = self.getDrive(url: "https://graph.microsoft.com/v1.0/me/drives/911f8a74cbcfe5cf/root/children", access_token: access_token, forHTTPHeaderField: "Authorization", httpMethod: "GET")
                    //print(json)
                    //print("\(access_token)")
                }
                catch let error as NSError {
                    print(error)
                }
                //print("responseString = \(responseString)")
            }
            task.resume()
        }
        
        decisionHandler(.allow)
    }
    
    func downloadFile(url: String) {
        
        if let audioUrl = URL(string: url) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            self.fileLinks.append(destinationUrl)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        
    }
    
    
    func getDrive(url: String, access_token: String, forHTTPHeaderField: String, httpMethod: String) ->Swift.Void {
        let get_url = URL(string: url)!
        var request = URLRequest(url: get_url)
        request.setValue("Bearer \(access_token)", forHTTPHeaderField: forHTTPHeaderField)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: String.Encoding.utf8)
            do {
                DispatchQueue.main.async {
                    //self.webView .removeFromSuperview()
                    self.datview.isHidden = true
                    self.ftable.isHidden = false
                    //access_token = json["access_token"] as! String
                }
            }
            catch let error as NSError {
                print(error)
            }
            //print("responseString = \(responseString!)")
            
            let jsonText = responseString
            var dictonary:NSDictionary?
            
            if let data = jsonText?.data(using: String.Encoding.utf8) {
                
                do {
                    dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as! NSDictionary
                    let valueDic = dictonary!["value"] as! NSArray
                    
                    //var fileNames = [] as Array
                    //var fileLinks = [] as Array
                    
                    for mansFails in valueDic {
                        let element = mansFails as! NSDictionary
                        let mansLinks = element["@microsoft.graph.downloadUrl"] as? String
                        let mansName = element["name"] as! String
                        //self.fileLinks.append(mansLinks)
                        self.fileNames.append(mansName)
                        
                        DispatchQueue.main.async {
                            self.ftable.reloadData()
                            print(mansName + " added to tableview")
                        }
                        
                        self.downloadFile(url: mansLinks!)
                        
                    }
                    
                    print(self.fileNames)
                } catch let error as NSError {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    
}

